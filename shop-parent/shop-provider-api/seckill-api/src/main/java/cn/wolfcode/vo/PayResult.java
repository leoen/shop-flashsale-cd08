package cn.wolfcode.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PayResult {

    private String outTradeNo;
    private String tradeNo;
    private String amount;
}
