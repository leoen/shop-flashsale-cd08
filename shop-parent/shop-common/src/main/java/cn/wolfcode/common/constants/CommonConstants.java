package cn.wolfcode.common.constants;


import java.util.Arrays;
import java.util.List;

public class CommonConstants {
    public static final String TOKEN_NAME = "token";
    public static final String REAL_IP = "X-REAL-IP";
    public static final String FEIGN_REQUEST_KEY = "FEIGN_REQUEST";
    public static final String REQUEST_TYPE_FEIGN = "1";
    public static final String REQUEST_TYPE_GATEWAY = "0";

    public static final List<String> REQUEST_TYPES = Arrays.asList(REQUEST_TYPE_FEIGN, REQUEST_TYPE_GATEWAY);
}
