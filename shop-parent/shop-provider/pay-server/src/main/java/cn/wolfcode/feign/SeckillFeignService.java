package cn.wolfcode.feign;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.vo.PayResult;
import cn.wolfcode.vo.PayVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("seckill-service")
public interface SeckillFeignService {

    @GetMapping("/orderPay/findPayVoByOrderNo")
    Result<PayVo> findPayVoByOrderNo(@RequestParam String orderNo);

    @PostMapping("/orderPay/paySuccess")
    Result<?> paySuccess(@RequestBody PayResult payResult);
}
