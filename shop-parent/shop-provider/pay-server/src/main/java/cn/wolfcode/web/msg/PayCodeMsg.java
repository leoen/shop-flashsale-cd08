package cn.wolfcode.web.msg;
import cn.wolfcode.common.web.CodeMsg;

/**
 * Created by wolfcode
 */
public class PayCodeMsg extends CodeMsg {
    private PayCodeMsg(Integer code, String msg){
        super(code,msg);
    }

    public static final PayCodeMsg PAY_ERROR = new PayCodeMsg(509500, "支付失败");
}
