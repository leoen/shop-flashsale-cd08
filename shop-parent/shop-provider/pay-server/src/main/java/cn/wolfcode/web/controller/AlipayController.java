package cn.wolfcode.web.controller;

import cn.wolfcode.common.exception.BusinessException;
import cn.wolfcode.common.web.Result;
import cn.wolfcode.common.web.anno.RequireLogin;
import cn.wolfcode.config.AlipayProperties;
import cn.wolfcode.domain.RefundVo;
import cn.wolfcode.feign.SeckillFeignService;
import cn.wolfcode.vo.PayResult;
import cn.wolfcode.vo.PayVo;
import cn.wolfcode.web.msg.PayCodeMsg;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeFastpayRefundQueryRequest;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeFastpayRefundQueryResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


@Slf4j
@RestController
@RequestMapping("/alipay")
public class AlipayController {
    private final AlipayClient alipayClient;
    private final AlipayProperties alipayProperties;
    private final SeckillFeignService seckillFeignService;

    public AlipayController(AlipayClient alipayClient, AlipayProperties alipayProperties, SeckillFeignService seckillFeignService) {
        this.alipayClient = alipayClient;
        this.alipayProperties = alipayProperties;
        this.seckillFeignService = seckillFeignService;
    }

    @PostMapping("/refund")
    public Result<Boolean> refund(@RequestBody RefundVo vo) {
        log.info("[支付宝支付] 发起退款请求: {}", JSON.toJSONString(vo));
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", vo.getOutTradeNo());
        bizContent.put("refund_amount", vo.getRefundAmount());
        bizContent.put("out_request_no", vo.getOutTradeNo());
        bizContent.put("refund_reason", vo.getRefundReason());

        // 设置参数
        request.setBizContent(bizContent.toString());

        AlipayTradeRefundResponse response = null;
        try {
            response = alipayClient.execute(request);
            log.info("[支付宝支付] 收到支付宝退款响应数据: {}", JSONObject.toJSONString(response));
            if (response.isSuccess()) {
                // 判断 fund_change == Y, 如果不相等, 需要进一步发起退款查询接口查询退款状态
                if (!"Y".equalsIgnoreCase(response.getFundChange())) {
                    boolean refunded = queryAlipayRefundStatus(vo.getOutTradeNo(), vo.getOutTradeNo());
                    return Result.success(refunded);
                }
                return Result.success(true);
            }
        } catch (AlipayApiException e) {
            throw new RuntimeException(e);
        }


        return Result.success(false);
    }

    private boolean queryAlipayRefundStatus(String outTradeNo, String requestNo) throws AlipayApiException {
        AlipayTradeFastpayRefundQueryRequest request = new AlipayTradeFastpayRefundQueryRequest();
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", outTradeNo);
        bizContent.put("out_request_no", requestNo);

        request.setBizContent(bizContent.toString());
        AlipayTradeFastpayRefundQueryResponse response = alipayClient.execute(request);
        log.info("[支付宝支付] 发起退款查询请求: {}", JSON.toJSONString(response));

        return response.isSuccess() && "REFUND_SUCCESS".equalsIgnoreCase(response.getRefundStatus());
    }

    @RequireLogin
    @PostMapping
    public Result<String> payment(String orderNo) {
        // 设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(alipayProperties.getReturnUrl());
        alipayRequest.setNotifyUrl(alipayProperties.getNotifyUrl());

        // 基于订单号查询订单信息
        Result<PayVo> result = seckillFeignService.findPayVoByOrderNo(orderNo);
        PayVo vo = result.getData(true);

        JSONObject json = new JSONObject();
        //商户订单号，商户网站订单系统中唯一订单号，必填
        json.put("out_trade_no", vo.getOutTradeNo());
        //付款金额，必填
        json.put("total_amount", vo.getTotalAmount());
        //订单名称，必填
        json.put("subject", vo.getSubject());
        //商品描述，可空
        json.put("body", vo.getBody());
        json.put("product_code", "FAST_INSTANT_TRADE_PAY");

        alipayRequest.setBizContent(json.toJSONString());

        // 请求
        try {
            String ret = alipayClient.pageExecute(alipayRequest).getBody();
            log.info("[支付宝支付] 发起支付响应结果：{}", ret);
            return Result.success(ret);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new BusinessException(PayCodeMsg.PAY_ERROR);
        }
    }

    @PostMapping("/notify_url")
    public String notifyUrl(HttpServletRequest request) {
        Map<String, String> params = this.resolveParams(request);
        log.info("[支付宝支付] 收到支付宝异步回调请求：{}", params);

        // 检查参数签名是否正确
        try {
            boolean signVerified = AlipaySignature.rsaCheckV1(params, alipayProperties.getAlipayPublicKey(), alipayProperties.getCharset(), alipayProperties.getSignType()); //调用SDK验证签名

            if (signVerified) {
                // 商户订单号
                String out_trade_no = params.get("out_trade_no");
                // 支付宝交易号
                String trade_no = params.get("trade_no");
                String total_amount = params.get("total_amount");
                // 交易状态
                String trade_status = params.get("trade_status");
                String appId = params.get("app_id");
                if (!alipayProperties.getAppId().equals(appId)) {
                    log.error("[支付宝支付] 异步回调 appId 不一致：notify_app_id={}, app_id={}", appId, alipayProperties.getAppId());
                    return "fail";
                }

                if (trade_status.equals("TRADE_FINISHED")) {
                    //退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
                    log.info("[支付宝支付] 收到订单完成消息通知：{}", params);
                } else if (trade_status.equals("TRADE_SUCCESS")) {
                    //付款完成后，支付宝系统发送该交易状态通知
                    PayResult payResult = new PayResult(out_trade_no, trade_no, total_amount);
                    Result<?> result = seckillFeignService.paySuccess(payResult);
                    if (result.hasError()) {
                        return "fail";
                    }
                }

                return "success";
            }
        } catch (AlipayApiException e) {
            // 签名验证失败，跳转到失败提示页面
            log.error("[支付宝支付] 异步回调验签失败：code={}, msg={}", e.getErrCode(), e.getErrMsg());
        }

        return "fail";
    }

    @GetMapping("/return_url")
    public void returnUrl(HttpServletRequest request, HttpServletResponse resp) throws IOException {
        Map<String, String> params = this.resolveParams(request);
        log.info("[支付宝支付] 收到支付宝同步回调请求：{}", params);

        // 检查参数签名是否正确
        try {
            boolean signVerified = AlipaySignature.rsaCheckV1(params, alipayProperties.getAlipayPublicKey(), alipayProperties.getCharset(), alipayProperties.getSignType()); //调用SDK验证签名

            if (signVerified) {
                // 验签成功
                String orderNo = params.get("out_trade_no");
                // 跳转到页面
                resp.sendRedirect(String.format(alipayProperties.getRedirectUrl(), orderNo));
                return;
            }
        } catch (AlipayApiException e) {
            // 签名验证失败，跳转到失败提示页面
            log.error("[支付宝支付] 同步回调验签失败：code={}, msg={}", e.getErrCode(), e.getErrMsg());
        }

        // 验签失败
        resp.sendRedirect("https://www.wolfcode.cn");
    }

    private Map<String, String> resolveParams(HttpServletRequest request) {
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            // 乱码解决，这段代码在出现乱码时使用
            // valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        return params;
    }
}
