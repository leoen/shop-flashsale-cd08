package cn.wolfcode.mq.listener;

import cn.wolfcode.domain.OperateIntergralVo;
import cn.wolfcode.mq.MQConstant;
import cn.wolfcode.service.IUsableIntegralService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@RocketMQMessageListener(
        consumerGroup = MQConstant.INTEGRAL_REFUND_CONSUMER_GROUP,
        topic = MQConstant.INTEGRAL_REFUND_TOPIC
)
@Component
@Slf4j
public class IntegralRefundMessageListener implements RocketMQListener<OperateIntergralVo> {

    @Autowired
    private IUsableIntegralService usableIntegralService;

    @Override
    public void onMessage(OperateIntergralVo vo) {
        log.info("[积分退款] 收到积分退款事务消息：{}", JSON.toJSONString(vo));
        usableIntegralService.doRefund(vo);
    }
}
