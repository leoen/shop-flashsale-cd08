package cn.wolfcode.web.controller;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.OperateIntergralVo;
import cn.wolfcode.service.IUsableIntegralService;
import io.seata.rm.tcc.api.BusinessActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/intergral")
public class IntegralController {
    @Autowired
    private IUsableIntegralService usableIntegralService;

    @PostMapping("/pay")
    public Result<String> integralPay(@RequestBody OperateIntergralVo vo) {
        BusinessActionContext context = new BusinessActionContext();
        Map<String, Object> map = new HashMap<>();
        map.put("vo", vo);
        context.setActionContext(map);
        String tradeNo = usableIntegralService.tryIntegralPay(vo, context);
        return Result.success(tradeNo);
    }

    @PostMapping("/refund")
    public Result<Boolean> refund(@RequestBody OperateIntergralVo vo) {
        usableIntegralService.doRefund(vo);
        return Result.success(true);
    }
}
