package cn.wolfcode.service;

import cn.wolfcode.domain.OperateIntergralVo;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;


@LocalTCC
public interface IUsableIntegralService {

    @TwoPhaseBusinessAction(useTCCFence = true, name = "tryIntegralPay", commitMethod = "confirmIntegralPay", rollbackMethod = "cancelIntegralPay")
    String tryIntegralPay(OperateIntergralVo vo, BusinessActionContext ctx);

    void confirmIntegralPay(BusinessActionContext ctx);

    void cancelIntegralPay(BusinessActionContext ctx);

    String integralPay(OperateIntergralVo vo);

    void doRefund(OperateIntergralVo vo);
}
