package cn.wolfcode.mq;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;

@Slf4j
public class DefaultSendCallback implements SendCallback {

    private String name;

    public DefaultSendCallback(String name) {
        this.name = name;
    }

    @Override
    public void onSuccess(SendResult sendResult) {
        log.info("[{}] 发送消息成功: {}", name, JSON.toJSONString(sendResult));
    }

    @Override
    public void onException(Throwable throwable) {
        log.warn("[{}] 消息发送失败: {}", name, throwable.getMessage());
    }
}
