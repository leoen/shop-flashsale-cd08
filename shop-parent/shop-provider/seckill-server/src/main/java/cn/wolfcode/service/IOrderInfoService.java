package cn.wolfcode.service;


import cn.wolfcode.common.domain.UserInfo;
import cn.wolfcode.domain.OrderInfo;
import cn.wolfcode.domain.SeckillProductVo;
import cn.wolfcode.mq.OrderTimeout;
import cn.wolfcode.vo.PayResult;

/**
 * Created by wolfcode
 */
public interface IOrderInfoService {

    OrderInfo selectByUserIdAndSeckillId(Long phone, Long seckillId, Integer time);

    String doSeckill(UserInfo userInfo, SeckillProductVo vo);

    void createOrderFallbackRedis(Long seckillId, Integer time, Long userPhone);

    void checkOrderPayTimeout(OrderTimeout message);

    OrderInfo findById(String orderNo);

    void paySuccess(PayResult result);

    void doRefund(String orderNo, Long phone);

    void commitRefund(String orderNo);

    void integralPay(String orderNo, Long phone);

    OrderInfo findByIdFromRedis(String orderNo);
}
