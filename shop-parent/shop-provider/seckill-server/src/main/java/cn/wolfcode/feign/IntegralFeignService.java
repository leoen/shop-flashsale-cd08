package cn.wolfcode.feign;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.OperateIntergralVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("intergral-service")
public interface IntegralFeignService {

    @PostMapping("/intergral/pay")
    Result<String> integralPay(@RequestBody OperateIntergralVo vo);

    @PostMapping("/intergral/refund")
    Result<Boolean> refund(@RequestBody OperateIntergralVo vo);
}
