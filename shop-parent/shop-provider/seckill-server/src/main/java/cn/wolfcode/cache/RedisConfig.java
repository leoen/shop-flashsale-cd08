package cn.wolfcode.cache;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.RedisScript;

import java.util.concurrent.ScheduledThreadPoolExecutor;

@Configuration
public class RedisConfig {

    @Bean
    public ScheduledThreadPoolExecutor scheduledThreadPoolExecutor() {
        return new ScheduledThreadPoolExecutor(Runtime.getRuntime().availableProcessors() * 2);
    }

    @Bean
    public RedisScript<Boolean> lockScript() {
        return RedisScript.of(new ClassPathResource("scripts/seckill_lock.lua"), Boolean.class);
    }

    @Bean
    public RedisScript<Boolean> unlockScript() {
        return RedisScript.of(new ClassPathResource("scripts/seckill_unlock.lua"), Boolean.class);
    }
}
