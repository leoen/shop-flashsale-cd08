package cn.wolfcode.mq.listener;

import cn.wolfcode.mq.MQConstant;
import cn.wolfcode.mq.OrderTimeout;
import cn.wolfcode.service.IOrderInfoService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@RocketMQMessageListener(
        consumerGroup = MQConstant.ORDER_PAY_TIMEOUT_CONSUMER_GROUP,
        topic = MQConstant.ORDER_PAY_TIMEOUT_TOPIC
)
@Component
@Slf4j
public class OrderPayTimeoutMQMessageListener implements RocketMQListener<OrderTimeout> {

    @Autowired
    private IOrderInfoService orderInfoService;

    @Override
    public void onMessage(OrderTimeout message) {
        log.info("[延迟消息] 收到订单超时未支付消息: {}", JSON.toJSONString(message));
        orderInfoService.checkOrderPayTimeout(message);
    }
}
