-- 获取锁
local lockKey = KEYS[1] -- 锁的key名称
local requestId = ARGV[1] -- 当前请求的ID
local expireTime = tonumber(ARGV[2]) -- 过期时间（单位为秒）

if redis.call("SETNX", lockKey, requestId) == 1 then
     redis.call("EXPIRE", lockKey, expireTime)
     return true
else
     return false
end
