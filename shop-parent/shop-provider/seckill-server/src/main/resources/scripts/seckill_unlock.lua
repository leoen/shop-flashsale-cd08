-- 获取锁
local lockKey = KEYS[1] -- 锁的key名称
local requestId = ARGV[1] -- 当前请求的ID

local value = redis.call("GET", lockKey)
if requestId == value then
    redis.call("DEL", lockKey)
    return true
end

return false
