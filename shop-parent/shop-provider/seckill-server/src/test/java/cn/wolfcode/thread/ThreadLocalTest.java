package cn.wolfcode.thread;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class ThreadLocalTest {

    private ThreadLocal<String> name = new ThreadLocal<>();
    private ThreadLocal<String> name1 = new ThreadLocal<>();

    @Test
    public void test() throws Exception {
        // ThreadLocal 会不会发生内存泄漏?
        // ThreadLocal 内存泄漏的原因是什么?
        // 怎么避免
        new Thread(() -> {
            name.set("线程一");
            name1.set("2222");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            doSomething();
        }).start();

        new Thread(() -> {
            name.set("线程二");
            doSomething();
        }).start();

        TimeUnit.SECONDS.sleep(9999);
    }

    public void doSomething() {
        System.out.println("名称：" + name.get());
    }
}
